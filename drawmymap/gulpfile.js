'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var babel = require('gulp-babel');

gulp.task('es6', function () {
    gulp.src('./assets/jsES6/*.js')
        .pipe(babel({
            ignore: 'gulpfile.js'
        }))
        .pipe(gulp.dest('./assets/js/'));
});

gulp.task('watch', function () {
    gulp.watch('./assets/jsES6/*.js', ['es6']);
    gulp.watch('./assets/scss/**/*.scss',['sass']);
});

gulp.task('sass', function () {
  return gulp.src('./assets/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./assets/css/'));
});
 


gulp.task('default', ['es6', 'watch']);