let delay = 100;
let addresses;
let nextAddress = 0;
let map;
let geocoder;
let autocomplete;
let infowindow;
let componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    country: 'long_name',
    postal_code: 'short_name'
  };

/**
 * MAP INITIALIZATION
 */ 
const initMap = () => {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 9,
        center: {lat: 48.854780, lng: 2.349797}
    });
    geocoder = new google.maps.Geocoder();
    
    infowindow = new google.maps.InfoWindow();

    initAutocomplete();
}

/**
 * AUTOCOMPLETE INITIALIZATION
 */
const initAutocomplete = () => {
    autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
    {types: ['geocode']});
    window.sessionStorage.setItem('autocomplete', autocomplete);
}

/**
 * géolocalisation - not used yet
 * purpose : allowing the user to geolocate himself instead of typing address
 */ 
const geolocate = () => {
    if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
        var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
        };
    });
    }
}

/**
 * is called when datas are received from the server. Start the marker process
 * @param {*} datas 
 */
const callback = (datas) => {
    addresses = getAddresses(datas);
    next();
}

/**
 * get the location from database
 */
const getLocalisations = () => {
    $.ajax({
        type: "get",
        url: "http://localhost:8000/api/v1/localisations",
        success: callback,
        dataType: "json"
    })
};

/**
 * transform result from DB to string
 * @param {Array} localisations all the addresse
 * @returns {Array} en aray of the stringified addresses 
 */
const getAddresses = (localisations) => localisations.map(({Adress, City, Zip_code})=> {
    const address = Adress + " " + City + " " + Zip_code;
    return address;
}) ;

/**
 * get the geolocalisation of a given address
 * @param {string} address 
 * @param {function} next 
 */
const getAddress = (address, next) => {
    geocoder.geocode({address:address}, (results,status) => {            
        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
            putOneMarker(results[0].geometry.location, address);
        } else {
            if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                nextAddress--;
                delay+=50;
            }  
        }
        next();
    }
    );
}

/**
 * put one marker on the map with a given location
 * @param {*} location coordinate
 * @param {*} content content of the marker infowindow
 */
const putOneMarker = (location, content) => {
    let marker = new google.maps.Marker({
        map: map,
        position: location
    });
    marker.addListener('click', function() {
        infowindow.setContent(content); 
        infowindow.open(map, marker);
      });

}

/**
 * it create each marker one by one with a delay in between
 * bypass google geocoding limitation
 * must not be used with a big database, not efficient
 * used because there is only 13 entries and I have 24h.
 */
const next = () => {
    if (nextAddress < addresses.length ) {
        nextAddress++;
        setTimeout(() => {getAddress(addresses[nextAddress], next)}, delay);
    }
}

/**
 * make some height calcul
 */
const calculateHeight = () => {
    $("#map").css('height', $(window).height() - $('nav').outerHeight() - $('footer').outerHeight())
}

$(document).ready( () => {
    $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyCT4TNSn5Bba222d4MdmAyyuyVVG2ktoyQ&libraries=geometry,places", function() {
        initMap ();
        getLocalisations();
     });
     
    $("#submitAddress").on('click', (ev) => {
        ev.preventDefault();
        const place = autocomplete.getPlace();
        const address = $('#autocomplete').val();
        if(place == null){
            geocoder.geocode({address:address}, (results,status) => {  
                if (status == google.maps.GeocoderStatus.OK && results[0].geometry.location_type !=='ROOFTOP' ) {
                    putOneMarker(results[0].geometry.location, address);
                }
            });            
        } else {
            const newPlace = place.address_components.map( (component) => {
                const addressType = component.types[0];
                if (componentForm[addressType]) {
                    return { addressType, value: component[componentForm[addressType]]}
                }
            })            
            putOneMarker(place.geometry.location, address); 
        }       
    })  
    calculateHeight();
    window.onresize = calculateHeight;
})