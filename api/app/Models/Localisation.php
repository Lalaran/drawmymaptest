<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Localisation extends Model {
    protected $table = 'localisation';
 
    protected $primaryKey = 'ID';
 
    public $timestamps = false;
    
    protected $guarded = [
        'ID'
    ];
}
