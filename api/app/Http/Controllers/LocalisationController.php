<?php

namespace App\Http\Controllers;
use App\Models\Localisation;

class LocalisationController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAll() {
        $localistations = Localisation::all();
        return response()->json($localistations);
    }
}
