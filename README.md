# Draw my Map

Affiche des markers sur une carte
j'ai joint un fichier pdf avec la justification de mes choix.

## Installation
## API
* Dans le dossier api/, executer les lignes de commande suivantes : `composer install`
* Modifier le fichier `.env` pour qu'il corresponde à la database.
```
DB_DATABASE=nomdelaBDD
DB_USERNAME=root
DB_PASSWORD=
```
* lancer le serveur SQL
* importer la BDD
* lancer le serveur php `php -S localhost:8000 -t public`

## Font
pour le front, pas d'installation nécessaire, sauf si l'on veut modifier le scss et le js.
Dans ce cas, il faut installer gulp, gulp-sass et gulp-babel

### Gulp
```
npm install gulp-cli -g
npm install gulp -D
npm install gulp-sass --save-dev
npm install --save-dev gulp-babel babel-core babel-preset-env
```
un fois installé, il suffit de se placer dans le dossier drawmymap et d'executer la commande `gulp`